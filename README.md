Project Information
Function:
Rest API 

How to test API:
Use POSTMAN or any tools

1. Get All Customers
Type: GET
Url: http://localhost:9090/api/customerTrans/getAll

2. Create New Customer
Type: POST
Url: http://localhost:9090/api/customerTrans/create
Data:
   {
   "customerId":"C0003",
   "accountNo":"104039392819",
   "description":"Aziz Abdul"
   }

3. Search Customers with pagination
Type: POST
Url: http://localhost:9090/api/customerTrans/search
Data:
   {
   "customerId": "C0001",
   "accountNo": "103929392919",
   "description": "Lee Chee Keong",
   "pages":2,
   "size":5
   }

Scheduled Job:
Refer to FileReadService
-Use  @Scheduled(initialDelay = 2000, fixedRate = 3000)
2000 = 2 seconds

