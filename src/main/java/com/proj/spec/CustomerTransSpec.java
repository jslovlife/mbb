package com.proj.spec;

import com.proj.model.CustomerTrans;
import com.proj.param.CustomerTransSearchParam;
import org.springframework.data.jpa.domain.Specification;
import cn.hutool.core.util.ObjectUtil;


public class CustomerTransSpec {

    public static Specification<CustomerTrans> createCustomerSpec(CustomerTransSearchParam customerTransSearchParam) {
        return accountNoLike(customerTransSearchParam.getAccountNo())
                .and(customerIdEquals(customerTransSearchParam.getCustomerId())
                        .and(descriptionLike(customerTransSearchParam.getDescription())));
    }

    public static Specification<CustomerTrans> accountNoLike(String accountNo) {
        return (root, query, criteriaBuilder)
                -> ObjectUtil.isNotNull(accountNo) ? criteriaBuilder.like(root.get("accountNo"), "%" + accountNo + "%") : criteriaBuilder.conjunction();
    }

    public static Specification<CustomerTrans> customerIdEquals(String customerId) {
        return (root, query, criteriaBuilder) ->
                ObjectUtil.isNotNull(customerId) ?
                        criteriaBuilder.equal(root.get("customerId"), customerId) : criteriaBuilder.conjunction();
    }

    public static Specification<CustomerTrans> descriptionLike(String description) {
        return (root, query, criteriaBuilder)
                -> ObjectUtil.isNotNull(description) ? criteriaBuilder.like(root.get("description"), "%" + description + "%") : criteriaBuilder.conjunction();
    }
}
