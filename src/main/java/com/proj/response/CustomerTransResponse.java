package com.proj.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerTransResponse
{
    private String customerId;
    private String accountNo;
    private String description;
    private double amount;
    private String trxDate;
    private String trxTime;
}