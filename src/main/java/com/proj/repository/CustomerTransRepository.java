package com.proj.repository;

import com.proj.model.CustomerTrans;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CustomerTransRepository extends CrudRepository<CustomerTrans, UUID>,
        JpaSpecificationExecutor<CustomerTrans> {
}
