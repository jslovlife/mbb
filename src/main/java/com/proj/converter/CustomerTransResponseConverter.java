package com.proj.converter;

import cn.hutool.core.bean.BeanUtil;
import com.proj.model.CustomerTrans;
import com.proj.response.CustomerTransResponse;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CustomerTransResponseConverter implements Converter<CustomerTrans, CustomerTransResponse>
{
    @Override
    public CustomerTransResponse convert(CustomerTrans customerTrans) {
        CustomerTransResponse customerTransResponse = CustomerTransResponse.builder().build();
        BeanUtil.copyProperties(customerTrans, customerTransResponse);

        return customerTransResponse;
    }
}
