package com.proj.param;

import lombok.Data;

@Data
public class CustomerTransSearchParam
{
    private String customerId;

    private String accountNo;

    private String description;

    private int page = 0;

    private int size = 1000;
}
