package com.proj.util;

public interface CommonConstant {

    String DATE_FORMAT_1 = "yyyy-MM-dd";

    String DATE_FORMAT_2 = "HH:mm:ss";
}
