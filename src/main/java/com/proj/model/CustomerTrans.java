package com.proj.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.proj.util.CommonConstant;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "customer_trans")
public class CustomerTrans {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private UUID id;

    @Column(name="CUSTOMER_ID", length=50, nullable=false, unique=false)
    private String customerId;

    @Column(name="ACCOUNT_NO", length=50, nullable=false, unique=false)
    private String accountNo;

    @Column(name="TRX_AMOUNT")
    private double amount;

    @Column(name="DESCRIPTION", length=200, nullable=false, unique=false)
    private String description;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern= CommonConstant.DATE_FORMAT_1)
    @Column(name = "TRX_DATE")
    private String trxDate;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern= CommonConstant.DATE_FORMAT_2)
    @Column(name = "TRX_TIME")
    private String trxTime;

}
