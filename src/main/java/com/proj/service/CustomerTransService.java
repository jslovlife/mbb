package com.proj.service;

import com.proj.model.CustomerTrans;
import com.proj.param.CustomerTransSearchParam;
import com.proj.repository.CustomerTransRepository;
import com.proj.response.CustomerTransResponse;
import com.proj.service.handler.SuccessResponsePaginationData;
import com.proj.spec.CustomerTransSpec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerTransService {

    @Autowired
    CustomerTransRepository customerTransRepository;

    @Autowired
    private ConversionService conversionService;

    public Iterable<CustomerTrans> getCustomers(){
        return customerTransRepository.findAll();
    }

    public SuccessResponsePaginationData search(CustomerTransSearchParam customerTransSearchParam){

        int page = customerTransSearchParam.getPage(); // Page = index 0 is page 1
        int size = customerTransSearchParam.getSize(); // Size = rows in a page
        // sorted descending having first record to be latest
        PageRequest pageRequest = PageRequest.of(page, size);

        // findAll with pagination
        Page<CustomerTrans> pageResult = customerTransRepository
                .findAll(CustomerTransSpec.createCustomerSpec(customerTransSearchParam), pageRequest);
        int totalPages = pageResult.getTotalPages();
        long totalElements = pageResult.getTotalElements();

        // convert to dto
        List<CustomerTrans> customerTrans = pageResult.getContent();
        List<CustomerTransResponse> customerTransRespons = customerTrans.stream()
                .map(customer -> conversionService.convert(customer, CustomerTransResponse.class))
                .collect(Collectors.toList());

        return new SuccessResponsePaginationData(totalPages, totalElements, customerTransRespons);

    }
}
