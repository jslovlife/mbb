package com.proj.service;

import com.proj.model.CustomerTrans;
import com.proj.repository.CustomerTransRepository;
import com.proj.util.CommonConstant;
import com.proj.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class FileReadService {

    @Autowired
    CustomerTransRepository customerTransRepository;

    @Scheduled(initialDelay = 2000, fixedRate = 3000)
    @Async
    public void refreshJob() throws Exception {

        InputStream inputStream = null;
        File file = new File(Constant.filePath + "testfile.txt");
        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            throw new Exception("No file found in folder");
        }

        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;

            List<CustomerTrans> customerTransList = new ArrayList<>();

            int i = 0;

            while ((line = br.readLine()) != null) {
                //Skip first line
                if( i != 0) {
                    String[] lineArr = line.split("\\|");
                    CustomerTrans customerTrans = new CustomerTrans();
                    customerTrans.setAccountNo(lineArr[0]);
                    customerTrans.setAmount(Double.parseDouble(lineArr[1]));
                    customerTrans.setDescription(lineArr[2]);
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                    customerTrans.setTrxDate(lineArr[3]);
                    customerTrans.setTrxTime(lineArr[4]);
                    customerTrans.setCustomerId(lineArr[5]);

                    customerTransList.add(customerTrans);
                }

                i++;

            }
            customerTransRepository.saveAll(customerTransList);

            System.out.println("Testing: "+ customerTransList.size());


        }catch (Exception e){
                e.printStackTrace();
        }

        file.renameTo(new File(Constant.archFilePath + "testfile.txt"));
    }
}
