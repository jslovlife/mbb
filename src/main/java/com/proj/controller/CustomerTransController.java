package com.proj.controller;

import com.proj.model.CustomerTrans;
import com.proj.param.CustomerTransSearchParam;
import com.proj.repository.CustomerTransRepository;
import com.proj.service.CustomerTransService;
import com.proj.service.handler.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/customerTrans")
public class CustomerTransController {

    @Autowired
    private CustomerTransRepository customerTransRepository;

    @Autowired
    private CustomerTransService customerTransService;

    @GetMapping("/getAll")
    public List<CustomerTrans> findAllCustomers() {

        return (List<CustomerTrans>) customerTransService.getCustomers();
    }

    @GetMapping("/{id}")
    public ResponseEntity<CustomerTrans> findUserById(@PathVariable(value = "id") UUID id) {
        Optional<CustomerTrans> customer = customerTransRepository.findById(id);

        if(customer.isPresent()) {
            return ResponseEntity.ok().body(customer.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/create")
    public CustomerTrans createCustomer(@Validated @RequestBody CustomerTrans customerTrans) {
        return customerTransRepository.save(customerTrans);
    }

    @PostMapping("/search")
    public ResponseData search(@RequestBody @Validated CustomerTransSearchParam customerTransSearchParam) {
        return customerTransService.search(customerTransSearchParam);
    }
}
